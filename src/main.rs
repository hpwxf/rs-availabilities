extern crate core;

#[derive(Debug, PartialEq)]
enum State {
    Available,
    NotAvailable,
}

#[derive(Debug, PartialEq)]
enum Periodicity {
    None,
    Unlimited { delay: usize },
    Limited { begin: usize, end: usize, delay: usize },
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Category(bool); // bool will be replaced by bitfield categories

struct Event {
    /// unit in hour starting on monday
    lower_bound: usize,
    // TODO mettre des range a..b
    duration: usize,
    /// starting from the lower_bound
    periodicity: Periodicity,
    state: State,
    category: Category,
}

impl Event {
    pub fn lower_bound(&self) -> usize { self.lower_bound }
    pub fn duration(&self) -> usize { self.duration }
    pub fn upper_bound(&self) -> usize { self.lower_bound + self.duration }
}


fn first_availability(event_stack: &Vec<Event>, lower_bound: usize, upper_bound: usize, duration: usize) -> Option<usize> {
    // TODO check lower_bound <= upper_bound && duration > 0
    match &event_stack[..] {
        [] => None,
        [e1] if e1.duration >= duration => Some(e1.lower_bound),
        [e1] if e1.duration < duration => None,
        _ => unimplemented!(),
    }
}

fn availabilities(event_stack: &Vec<Event>, lower_bound: usize, upper_bound: usize) -> Vec<Category> {
    // TODO check lower_bound <= upper_bound
    let mut agenda = vec![Category(false); upper_bound - lower_bound];
    for event in event_stack {
        match event.periodicity {
            Periodicity::None => {
                let upper_bound = usize::min(upper_bound - lower_bound, event.upper_bound() - lower_bound);
                let lower_bound = usize::max(0, event.lower_bound() - lower_bound);
                for i in lower_bound..upper_bound {
                    agenda[i] = event.category;
                }
            }
            Periodicity::Unlimited { delay } => {
                // find all k s.t. [lower_bound, upper_bound] /\ [event.lower_bound, event.upper_bound] + k * periodicity.delay != Ø
                let min_k = (lower_bound as i32 - event.upper_bound() as i32 + delay as i32 - 1) / delay as i32; // event.upper_bound + k * delay > lower_bound 
                let max_k = (upper_bound as i32 - event.lower_bound() as i32) / delay as i32; // event.lower_bound + k * delay < upper_bound
                // println!("min_k: {}, max_k: {}", min_k, max_k);
                for k in min_k..=max_k {
                    let k = k as usize;
                    let upper_bound = usize::min(upper_bound - lower_bound, event.upper_bound() + k * delay - lower_bound);
                    let lower_bound = usize::max(0, event.lower_bound() + k * delay - lower_bound);
                    // println!("{} {}", lower_bound, upper_bound);
                    for i in lower_bound..upper_bound {
                        agenda[i] = event.category;
                    }
                }
            }
            Periodicity::Limited { begin, end, delay } => {
                // like Periodicity::Unlimited with /\ [begin, end]
                let lower_bound = usize::max(lower_bound, begin);
                let upper_bound = usize::min(upper_bound, end);
                unimplemented!()
            }
        }
    }
    agenda
}

// const ONE_DAY: usize = 24;
// const ONE_WEEK: usize = ONE_DAY * 7;

const ONE_DAY: usize = 4;
const ONE_WEEK: usize = ONE_DAY * 3;

fn main() {
    // let _event_stack = vec![
    //     Event { lower_bound: 32, duration: 8, periodicity: Periodicity::Unlimited { delay: ONE_WEEK }, state: State::Available, category: Category(true) },
    //     Event { lower_bound: 56, duration: 4, periodicity: Periodicity::Unlimited { delay: ONE_WEEK }, state: State::Available, category: Category(true) },
    //     Event { lower_bound: 32, duration: 8, periodicity: Periodicity::Unlimited { delay: 2 * ONE_WEEK }, state: State::NotAvailable, category: Category(true) },
    //     Event { lower_bound: 56 + ONE_WEEK + 2, duration: 2, periodicity: Periodicity::None, state: State::NotAvailable, category: Category(true) },
    // ];
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_stack_should_not_have_availability() {
        let event_stack = Vec::<Event>::new();
        assert!(first_availability(&event_stack, 0, 2 * ONE_WEEK, 1).is_none());
        assert_eq!(&availabilities(&event_stack, 0, 2 * ONE_WEEK)[..], &vec![Category(false); 2 * ONE_WEEK][..]);
    }

    #[test]
    fn single_periodic_event_should_have_availability_if_not_too_large() {
        let event_stack = vec![
            Event { lower_bound: ONE_DAY, duration: ONE_DAY / 2, periodicity: Periodicity::Unlimited { delay: ONE_WEEK }, state: State::Available, category: Category(true) }
        ];
        assert_eq!(first_availability(&event_stack, 0, 2 * ONE_WEEK, 1), Some(ONE_DAY));
        // assert_eq!(first_availability(&event_stack, 0, 2 * ONE_WEEK, ONE_DAY / 2 + 1), None);
    }

    #[test]
    fn single_periodic_event_should_have_periodic_agenda1() {
        let event_stack = vec![
            Event { lower_bound: ONE_DAY, duration: ONE_DAY / 2, periodicity: Periodicity::Unlimited { delay: ONE_WEEK }, state: State::Available, category: Category(true) }
        ];
        let mut agenda = vec![Category(false); 2 * ONE_WEEK];
        let event = &event_stack[0];
        for i in event.lower_bound()..event.upper_bound() {
            agenda[i] = Category(true);
            agenda[i + ONE_WEEK] = Category(true)
        }

        let first_week_availabilities = availabilities(&event_stack, 0, ONE_WEEK);
        assert_eq!(&first_week_availabilities[..], &agenda[0..ONE_WEEK]);
        let second_week_availabilities = availabilities(&event_stack, ONE_WEEK, 2 * ONE_WEEK);
        assert_eq!(&second_week_availabilities[..], &agenda[ONE_WEEK..2 * ONE_WEEK]);
        let two_weeks_availabilities = availabilities(&event_stack, 0, 2 * ONE_WEEK);
        assert_eq!(&two_weeks_availabilities[..], &agenda[0..2 * ONE_WEEK]);
    }
}